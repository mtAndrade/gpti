<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Task extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'tasks';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function main_category(){
        return $this->belongsTo('App\Models\Category');
    }

    public function project(){
        return $this->belongsTo('App\Models\Project');
    }

    public function tags(){
        return $this->belongsToMany('App\Models\Tag', 'tasks_x_tags', 'task_id', 'tag_d');
    }

    public function categories(){
        return $this->belongsToMany('App\Models\Categoy', 'tasks_x_categories', 'task_id', 'category_id');
    }

    public function users(){
        return $this->belongsToMany('App\User', 'tasks_x_users', 'task_id', 'user_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
