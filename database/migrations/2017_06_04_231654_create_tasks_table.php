<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("tasks", function (Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->string("description");
            $table->integer("function_points");
            $table->integer("project_id")->unsigned();
            $table->integer("main_category_id")->unsigned();
            $table->timestamps();

            $table->foreign("project_id")->references("id")->on("projects");
            $table->foreign("main_category_id")->references("id")->on("categories");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("tasks");
    }
}
