<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("projects", function (Blueprint $table){
            $table->increments("id");
            $table->string("name");
            $table->integer("owner_id")->unsigned();
            $table->string("description");
            $table->dateTime('deadline');
            $table->timestamps();

            $table->foreign("owner_id")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("projects");
    }
}
