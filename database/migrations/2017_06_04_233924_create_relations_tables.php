<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationsTables extends Migration
{
    public function up()
    {
        Schema::create("tasks_x_categories", function (Blueprint $table){
            $table->integer("task_id")->unsigned();
            $table->integer("category_id")->unsigned();

            $table->foreign("task_id")->references('id')->on('tasks');
            $table->foreign("category_id")->references('id')->on('categories');
        });

        Schema::create("tasks_x_tags", function (Blueprint $table){
            $table->integer("task_id")->unsigned();
            $table->integer("tag_id")->unsigned();

            $table->foreign("task_id")->references('id')->on('tasks');
            $table->foreign("tag_id")->references('id')->on('tags');
        });

        Schema::create("tasks_x_users", function (Blueprint $table){
            $table->integer("task_id")->unsigned();
            $table->integer("user_id")->unsigned();

            $table->foreign("task_id")->references('id')->on('tasks');
            $table->foreign("user_id")->references('id')->on('users');
        });

        Schema::create("projects_x_users", function (Blueprint $table){
            $table->integer("user_id")->unsigned();
            $table->integer("project_id")->unsigned();

            $table->foreign("user_id")->references('id')->on('users');
            $table->foreign("project_id")->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("tasks_x_categories");
        Schema::drop("tasks_x_tags");
        Schema::drop("tasks_x_users");
        Schema::drop("projects_x_tasks");
    }
}
