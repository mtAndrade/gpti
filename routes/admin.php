<?php

    CRUD::resource('tag', 'TagCrudController');
    CRUD::resource('category', 'CategoryCrudController');
    CRUD::resource('report', 'ReportCrudController');
    CRUD::resource('project', 'ProjectCrudController');
    CRUD::resource('task', 'TaskCrudController');